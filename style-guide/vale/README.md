# Salt Style Guide

This directory contains a style guide for use with [vale](https://github.com/errata-ai/vale). It's meant to be used to extend vale's [implementation](https://github.com/errata-ai/Google) based on the [Google Developer Documentation Style Guide](https://developers.google.com/style/)

To use this style guide in a document repository, create the following directory structure:

```
$ tree -a
.
├── .vale
│   └── styles
│       ├── Google
│       ├── Salt
│       └── Vocab
│           └── Salt
└── .vale.ini
```
